package tk.maskedredstonerproz.plugins

import io.ktor.serialization.gson.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.application.*

/**
 * The serialization configuration
 * @author MaskedRedstonerProZ
 */
fun Application.configureSerialization() {
    install(ContentNegotiation) {
        gson { }
    }
}
