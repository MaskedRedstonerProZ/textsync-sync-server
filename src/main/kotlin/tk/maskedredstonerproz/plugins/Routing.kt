package tk.maskedredstonerproz.plugins

import tk.maskedredstonerproz.data.repos.device.DeviceRepository
import tk.maskedredstonerproz.data.repos.file.FileRepository
import tk.maskedredstonerproz.routes.*
import io.ktor.server.routing.*
import io.ktor.server.application.*
import org.koin.ktor.ext.inject
/**
 * The routing configuration
 * @author MaskedRedstonerProZ
 */
fun Application.configureRouting() {

    val deviceRepository: DeviceRepository by inject()
    val fileRepository: FileRepository by inject()

    routing {

        addDevice(deviceRepository)

        removeDevice(deviceRepository)

        addFile(fileRepository)

        removeFile(fileRepository)

        startSyncingUnsyncedFiles(fileRepository)

        stopSyncingASyncedFile(fileRepository)

        syncUnsyncedFiles(fileRepository)

    }
}
