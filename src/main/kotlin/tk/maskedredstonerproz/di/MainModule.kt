package tk.maskedredstonerproz.di

import com.google.gson.Gson
import tk.maskedredstonerproz.data.repos.device.DeviceRepository
import tk.maskedredstonerproz.data.repos.device.DeviceRepositoryImpl
import tk.maskedredstonerproz.data.repos.file.FileRepository
import tk.maskedredstonerproz.data.repos.file.FileRepositoryImpl
import tk.maskedredstonerproz.util.Constants
import org.koin.dsl.module
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo

/**
 * Main Koin module for dependency injection
 * @author MaskedRedstonerProZ
 */
val mainModule = module {

    /**
     * MongoDB database dependency definition
     * @author MaskedRedstonerProZ
     */
    single {
        val url = System.getenv("DATABASE_URL")?: "mongodb://mongo:27017"
        val client = KMongo.createClient(url).coroutine
        client.getDatabase(Constants.DATABASE_NAME)
    }

    /**
     * [DeviceRepository] dependency definition
     * @author MaskedRedstonerProZ
     */
    single<DeviceRepository> {
        DeviceRepositoryImpl(get())
    }

    /**
     * [FileRepository] dependency definition
     * @author MaskedRedstonerProZ
     */
    single<FileRepository> {
        FileRepositoryImpl(get())
    }

    /**
     * [Gson] database dependency definition
     * @author MaskedRedstonerProZ
     */
    single { Gson() }
}