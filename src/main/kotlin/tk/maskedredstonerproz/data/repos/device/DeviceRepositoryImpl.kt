package tk.maskedredstonerproz.data.repos.device

import tk.maskedredstonerproz.data.models.Device
import org.litote.kmongo.coroutine.CoroutineDatabase

/**
 * The [DeviceRepository] implementation
 * @param db The database the [DeviceRepository] interacts with
 * @author MaskedRedstonerProZ
 */
class DeviceRepositoryImpl(
    db: CoroutineDatabase
): DeviceRepository {

    /**
     * The [Device] collection in the database
     * @author MaskedRedstonerProZ
     */
    private val devices = db.getCollection<Device>()

    override suspend fun addDevice(device: Device) {
        devices.insertOne(device)
    }

    override suspend fun getDeviceById(id: String): Device? = devices.findOneById(id)

    override suspend fun removeDevice(deviceId: String, onUnableToRemove: suspend () -> Unit) {

        getDeviceById(deviceId)?.let { device: Device ->
             devices.deleteOneById(device.identifier)
             return
         }

        onUnableToRemove()
    }
}