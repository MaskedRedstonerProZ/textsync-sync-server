package tk.maskedredstonerproz.data.repos.file

import tk.maskedredstonerproz.data.models.*

/**
 * The [File] repository used to interact with the database
 * @author MaskedRedstonerProZ
 */
interface FileRepository {

    /**
     * Adds a [File] to the database
     * @param file The [File] that is added
     * @author MaskedRedstonerProZ
     */
    suspend fun addFile(file: File): Boolean

    /**
     * Gets a single [File] from the database according to it's [id]
     * @param id The [id] of the [File] to get
     * @return The [File] with the corresponding [id], null if there's no such [File]
     * @author MaskedRedstonerProZ
     */
    suspend fun getFileById(id: String): File?

    /**
     * Adds a [Device] id to all the [File]s's arrays of devices a particular [File] is synchronised to
     * @param deviceId The id that is added
     * @author MaskedRedstonerProZ
     */
    suspend fun addDeviceSyncedTo(deviceId: String)

    /**
     * Removes a [Device] id from the [File] (denoted by the [fileId])'s array of devices a particular [File] is synchronised to
     * @param deviceId The id that is removed
     * @param fileId The id of the [File] from which the [deviceId] is removed
     * @author MaskedRedstonerProZ
     */
    suspend fun removeDeviceSyncedTo(deviceId: String, fileId: String)

    /**
     * Gets a [List] of all the [File]s that are not synchronised
     * to the corresponding [Device] from the database
     * @param deviceId The id of the [Device] the [File]s aren't synchronised to
     * @return A [List] of all the [File]s that are not synchronised
     * to the corresponding [Device]
     * @author MaskedRedstonerProZ
     */
    suspend fun getAllSyncableFiles(deviceId: String): List<File>

    /**
     * Removes a [File] from the database
     * @param fileId The id of the [File] that is removed
     * @param onUnableToRemove Called if there's no [File] with the given [fileId]
     * @author MaskedRedstonerProZ
     */
    suspend fun removeFile(fileId: String, onUnableToRemove: suspend () -> Unit)

}