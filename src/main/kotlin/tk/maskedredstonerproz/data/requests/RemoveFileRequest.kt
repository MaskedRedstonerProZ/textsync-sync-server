package tk.maskedredstonerproz.data.requests

import io.ktor.server.routing.Route
import tk.maskedredstonerproz.data.models.File

/**
 * Request data class that represents a request on the file deletion [Route]
 * @param fileId The id of the [File] to remove
 * @author MaskedRedstonerProZ
 */
data class RemoveFileRequest(
    val fileId: String
)
