package tk.maskedredstonerproz.data.requests

import tk.maskedredstonerproz.data.models.*

/**
 * Request data class that represents a [Device] upload or deletion request
 * @param deviceId The id of the [Device] being uploaded or deleted
 * @author MaskedRedstonerProZ
 */
data class DeviceAddOrRemoveRequest(
    val deviceId: String,
)
