package tk.maskedredstonerproz.data.responses

import tk.maskedredstonerproz.data.requests.*
import tk.maskedredstonerproz.data.models.*

/**
 * Response data class that represents a response to a [FileSyncRequest]
 * @param files The [Array] of [File]s in the form of [ByteArray]s
 * @param originalFileNames The [Array] of names of the corresponding [File]s
 * @param devicesOfOrigin The [Array] of ids of the [Device]s the corresponding [File]s came from
 * @param fileIds The ids of the corresponding [File]s
 * @author MaskedRedstonerProZ
 */
data class FileSyncResponse(
    val files: Array<ByteArray>,
    val originalFileNames: Array<String>,
    val devicesOfOrigin: Array<String>,
    val fileIds: Array<String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FileSyncResponse

        if (!files.contentEquals(other.files)) return false
        if (!originalFileNames.contentEquals(other.originalFileNames)) return false
        if (!devicesOfOrigin.contentEquals(other.devicesOfOrigin)) return false
        if (!fileIds.contentEquals(other.fileIds)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = files.contentHashCode()
        result = 31 * result + originalFileNames.contentHashCode()
        result = 31 * result + devicesOfOrigin.contentHashCode()
        result = 31 * result + fileIds.contentHashCode()
        return result
    }
}
