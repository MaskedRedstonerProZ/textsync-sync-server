package tk.maskedredstonerproz.data.responses

import tk.maskedredstonerproz.data.requests.*
import tk.maskedredstonerproz.data.models.*

/**
 * Response data class that represents a response to an [AddFileRequest]
 * @param fileId The id of the added [File]'s info in the database
 * @author MaskedRedstonerProZ
 */
data class AddFileResponse(
    val fileId: String
)
