package tk.maskedredstonerproz

import tk.maskedredstonerproz.di.mainModule
import io.ktor.server.application.*
import tk.maskedredstonerproz.plugins.*
import org.koin.ktor.plugin.Koin

/**
 * Kotlin main function, entrypoint of the project
 * @param args The [Array] of arguments passed to it
 * @author MaskedRedstonerProZ
 */
fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

/**
 * The Ktor application plugin configuration module
 * @author MaskedRedstonerProZ
 */
@Suppress("unused") // application.conf references the main function. This annotation prevents the IDE from marking it as unused.
fun Application.module() {
    install(Koin) {
        modules(mainModule)
    }
    configureHTTP()
    configureSerialization()
    configureMonitoring()
    configureRouting()
}
