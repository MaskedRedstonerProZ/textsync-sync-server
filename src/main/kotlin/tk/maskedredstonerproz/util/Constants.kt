package tk.maskedredstonerproz.util

/**
 * The project [Constants] object
 * @author MaskedRedstonerProZ
 */
object Constants {

    /**
     * The database name constant
     * @author MaskedRedstonerProZ
     */
    const val DATABASE_NAME = "textpad-db"

    /**
     * The file path constant
     * @author MaskedRedstonerProZ
     */
    const val FILE_PATH = "/app/files/"

    /**
     * The device id query parameter constant
     * @author MaskedRedstonerProZ
     */
    const val PARAMETER_DEVICE_ID = "device_id"
}