FROM amazoncorretto:19.0.1
EXPOSE 8080:8080
RUN mkdir /app && mkdir /app/project
COPY . /app/project/
RUN cd /app/project && ./gradlew buildFatJar && cp /app/project/build/libs/textsync-sync-server.jar /app/textsync-sync-server.jar && rm -rf /app/project
ENTRYPOINT ["java","-jar","/app/textsync-sync-server.jar"]